## Subsurface Test: Agricultural

This is the crop land irrigation problem reported in https://doi.org/10.1016/j.agwat.2018.08.005. It models complex hydrological processes in the crop fields including irrigation, rainfall, evapotranspiration, root water uptake and free drainage