"""
    Validate Serghei against crop irrigation problem
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.io import netcdf
import reference as ref

lgd = ['Measured','Hydrus2D','SERGHEI-GW(PC)','SERGHEI-GW(MP)']
	
fdir = ['out14PC/','out15PC/','out14MP/','out15MP/']

ind = [20,40,60]
dt = 1
"""
    --------------------------------------------------------------------
                         Read Serghei results
    --------------------------------------------------------------------
"""
h_out = []
wc_out = []
for ff in range(len(fdir)):
    fname = fdir[ff] + 'output_subsurface.nc'
    fid = netcdf.NetCDFFile(fname,'r')
    h_out.append(fid.variables['hd'])
    wc_out.append(fid.variables['wc'])
    fid.close()
    print(' Shape of the output fields: ', np.shape(h_out[ff]))

serghei20 = []
serghei40 = []
serghei60 = []
time = []
for ff in range(len(fdir)):
	dim = np.shape(wc_out[ff])
	serghei20.append([])
	serghei40.append([])
	serghei60.append([])
	for ii in range(dim[0]):
		serghei20[-1].append(wc_out[ff][ii,20,0,50])
		serghei40[-1].append(wc_out[ff][ii,40,0,50])
		serghei60[-1].append(wc_out[ff][ii,60,0,50])
	time.append(np.linspace(0,(dim[0]-1)*dt,dim[0]))
print(time)

"""
    --------------------------------------------------------------------
                                Read measurements
    --------------------------------------------------------------------
"""
wc2014, wc4014, wc6014 = ref.get_measured(2014)
wc20hydrus14, wc40hydrus14, wc60hydrus14 = ref.get_modeled(2014)
wc2015, wc4015, wc6015 = ref.get_measured(2015)
wc20hydrus15, wc40hydrus15, wc60hydrus15 = ref.get_modeled(2015)

wc20 = [wc2014, wc2015]
wc40 = [wc4014, wc4015]
wc60 = [wc6014, wc6015]
wc20hydrus = [wc20hydrus14,wc20hydrus15]
wc40hydrus = [wc40hydrus14,wc40hydrus15]
wc60hydrus = [wc60hydrus14,wc60hydrus15]

"""
    --------------------------------------------------------------------
                                Make plot
    --------------------------------------------------------------------
"""


#color = [[223,223,234],[141,159,209],[209,140,184],[34,69,162],[143,38,126],[1,1,1]]
color = [[141,159,209],[141,159,209],[209,140,184],[34,69,162],[143,38,126]]
for ii in range(len(color)):
    for jj in range(3):
        color[ii][jj] = color[ii][jj] / 255
fs = 9

cm2inch = 1.0/2.54

img = plt.figure(1, figsize=[18*cm2inch, 12*cm2inch])
ff = 0
#	Subplot 1
plt.subplot(2,2,1)
ax = img.gca()
pos1 = ax.get_position()
pos2 = [pos1.x0-0.015, pos1.y0+0.02, pos1.width*1.1, pos1.height*1.15]
ax.set_position(pos2)
plt.scatter(wc20[ff][:,0],wc20[ff][:,1],s=40,c='k',marker='x')
plt.plot(wc20hydrus[ff][:,0],wc20hydrus[ff][:,1],color='k')
plt.plot(time[ff], np.array(serghei20[ff]),color=color[ff])
plt.plot(time[ff+2], np.array(serghei20[ff+2]),color=color[ff+2],linestyle='--')
plt.xticks([],[])
#plt.xlabel('Time [day]',fontsize=fs)
plt.ylabel('Water Content',fontsize=fs)
plt.ylim([0.1,0.3])
plt.yticks([0.1,0.15,0.2,0.25,0.3],[0.1,0.15,0.2,0.25,0.3],fontsize=fs)
plt.text(0,0.28,'(a) 2014 Depth = 0.2m',fontsize=fs)

#	Subplot 2
plt.subplot(2,2,2)
ax = img.gca()
pos1 = ax.get_position()
pos2 = [pos1.x0+0.015, pos1.y0+0.02, pos1.width*1.1, pos1.height*1.15]
ax.set_position(pos2)
plt.scatter(wc60[ff][:,0],wc60[ff][:,1],s=40,c='k',marker='x')
plt.plot(wc60hydrus[ff][:,0],wc60hydrus[ff][:,1],color='k')
plt.plot(time[ff], np.array(serghei60[ff]),color=color[ff])
plt.plot(time[ff+2], np.array(serghei60[ff+2]),color=color[ff+2],linestyle='--')
plt.ylim([0.2,0.4])
plt.xticks([],[])
plt.yticks([0.2,0.25,0.3,0.35,0.4],[0.2,0.25,0.3,0.35,0.4],fontsize=fs)
plt.text(0,0.38,'(b) 2014 Depth = 0.6m',fontsize=fs)

ff += 1

#	Subplot 3
plt.subplot(2,2,3)
ax = img.gca()
pos1 = ax.get_position()
pos2 = [pos1.x0-0.015, pos1.y0, pos1.width*1.1, pos1.height*1.15]
ax.set_position(pos2)
plt.scatter(wc20[ff][:,0],wc20[ff][:,1],s=40,c='k',marker='x')
plt.plot(wc20hydrus[ff][:,0],wc20hydrus[ff][:,1],color='k')
plt.plot(time[ff], np.array(serghei20[ff]),color=color[ff])
plt.plot(time[ff+2], np.array(serghei20[ff+2]),color=color[ff+1],linestyle='--')
plt.xlabel('Time [day]',fontsize=fs)
plt.xticks([0,40,80,120],[0,40,80,120],fontsize=fs)
plt.ylabel('Water Content',fontsize=fs)
plt.ylim([0.1,0.3])
plt.yticks([0.1,0.15,0.2,0.25,0.3],[0.1,0.15,0.2,0.25,0.3],fontsize=fs)
plt.text(0,0.28,'(c) 2015 Depth = 0.2m',fontsize=fs)

#	Subplot 4
plt.subplot(2,2,4)
ax = img.gca()
pos1 = ax.get_position()
pos2 = [pos1.x0+0.015, pos1.y0, pos1.width*1.1, pos1.height*1.15]
ax.set_position(pos2)
plt.scatter(wc60[ff][:,0],wc60[ff][:,1],s=40,c='k',marker='x')
plt.plot(wc60hydrus[ff][:,0],wc60hydrus[ff][:,1],color='k')
plt.plot(time[ff], np.array(serghei60[ff]),color=color[ff])
plt.plot(time[ff+2], np.array(serghei60[ff+2]),color=color[ff+1],linestyle='--')
plt.xlabel('Time [day]',fontsize=fs)
plt.ylim([0.2,0.4])
plt.xticks([0,40,80,120],[0,40,80,120],fontsize=fs)
plt.yticks([0.2,0.25,0.3,0.35,0.4],[0.2,0.25,0.3,0.35,0.4],fontsize=fs)
plt.text(0,0.38,'(d) 2015 Depth = 0.6m',fontsize=fs)
plt.legend(lgd, fontsize=fs)

plt.savefig("t6-results.eps", format='eps')


plt.show()
